﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace UsbMaker
{
    class UsbVolume
    {

        const string fs = "FAT32";
        const bool quick = true;
        int clusterSize = 4096;
        string label = "Goodbytes";
        bool compression = false;


        ManagementObject usbVolume; // the Win32_Volume Object this class represents
        string deviceID;

        public string DriveLetter { get { return usbVolume["DriveLetter"].ToString(); } }

        public UsbVolume(ManagementObject usb_volume)
        {
            this.usbVolume = usb_volume;
            deviceID = usbVolume["DeviceID"].ToString();
        }

        public override string ToString()
        {
            return $"{usbVolume["Label"] ?? "NO LABEL":s} ({usbVolume["DriveLetter"] ?? "":s}) {((ulong)usbVolume["Capacity"] / 1073741824.00):f2}GB"; //1073741824 = number of bytes in a GB
        }

        public void Format()
        {
            int result;
            try
            {

                result = Convert.ToInt32(usbVolume.InvokeMethod("Format", new object[] { fs, quick, clusterSize, label, compression }));

            }
            catch (Exception e)
            {
                throw new Exception("Issue formatting volume " + this.ToString(), e);
            }

            // If format method doesn't return successful throw an exception and pass the value up
            if ( result != 0)
            {
                throw new Exception($"Failed to format volume {this.ToString():s}  Win32.Format returned {result:i}");
            }
        }
    }
}

﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;


namespace UsbMaker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<UsbVolume> usb_volumes;
        public MainWindow()
        {
            InitializeComponent();
            Properties.Settings.Default.Upgrade();

            // Get list of usb_volumes and tie to the list box
            usb_volumes = Controller.GetUsbVolumes();
            lstbox_usbs.ItemsSource = usb_volumes;
        }

        private async void btn_create_Click(object sender, RoutedEventArgs eArgs)
        {
            // Ask for confirmation
            MessageBoxResult messageBoxResult = MessageBox.Show("WARNING. The selected volumes will be formatted if you continue. Format seleceted volumes?", "WARNING", System.Windows.MessageBoxButton.OKCancel);
            if (messageBoxResult == MessageBoxResult.Cancel)
                return; // exit function if they hit cancel


            var progressIndicator = new Progress<Tuple<string, int>>(ReportProgress);
            DisableControls();
            // Disable the UI while we do stuff


            try
            {
                // Format selected volumes
                ShowProgressIndicators("Formatting", lstbox_usbs.SelectedItems.Count);
                await Controller.FormatUsbVolumes(lstbox_usbs.SelectedItems, progressIndicator);

                // Copy source to volumes
                ShowProgressIndicators("Copying", Controller.GetDirectorySize(txtbx_src.Text));
                List<string> selectedDriveLetters = GetSelectedDriveLetters();
                await Controller.CopyToMany(txtbx_src.Text, selectedDriveLetters, progressIndicator);
            }
            catch (Exception e)
            {
                MessageBox.Show("An exception occured! Program will now close. \n" + e, "ERROR!", MessageBoxButton.OK);
                Application.Current.Shutdown();
            }

            //Report Completion
            System.Media.SystemSounds.Asterisk.Play();
            MessageBox.Show("All Done!", "Finished", System.Windows.MessageBoxButton.OK);

            // exit
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Pulls up a file browser dialog and shoves the path into our source textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_browse_Click(object sender, RoutedEventArgs e)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog
            {
                IsFolderPicker = true
            };
            CommonFileDialogResult result = dialog.ShowDialog();
            if (result == CommonFileDialogResult.Ok)
            {
                txtbx_src.Text = dialog.FileName;
            }
        }

        /// <summary>
        /// Updates our UI on our current progress
        /// </summary>
        /// <param name="progress"></param>
        private void ReportProgress(Tuple<string, int> progress)
        {
            lbl_current_file.Content = progress.Item1;
            progress_bar.Value += progress.Item2;
        }

        private List<string> GetSelectedDriveLetters()
        {
            List<string> selectedDriveLetters = new List<string>();
            foreach (UsbVolume selected in lstbox_usbs.SelectedItems)
            {
                selectedDriveLetters.Add(selected.DriveLetter);
            }
            return selectedDriveLetters;
        }

        /// <summary>
        /// Disables form controls while we are doing stuff
        /// </summary>
        private void DisableControls()
        {
            btn_browse.IsEnabled = false;
            btn_create.IsEnabled = false;
            txtbx_src.IsEnabled = false;
        }

        /// <summary>
        /// Enables form controls. Currently unused.
        /// </summary>
        private void EnableControls()
        {
            btn_browse.IsEnabled = true;
            btn_create.IsEnabled = true;
            txtbx_src.IsEnabled = true;
        }

        /// <summary>
        /// Hides progress indicators (progress bar, current file)
        /// </summary>
        private void HideProgressIndicators()
        {
            lbl_operation.Visibility = Visibility.Hidden;
            lbl_current_file.Visibility = Visibility.Hidden;
            progress_bar.Visibility = Visibility.Hidden;
        }




        private void ShowProgressIndicators(string operation, long progressMax)
        {
            lbl_operation.Visibility = Visibility.Visible;
            lbl_operation.Content = operation;
            lbl_current_file.Visibility = Visibility.Visible;
            progress_bar.Visibility = Visibility.Visible;
            progress_bar.Maximum = progressMax;
            progress_bar.Value = 0;
        }


        /// <summary>
        /// When the source textbox value changes this saves it as a user setting so it is remembered between sessions.
        /// </summary>
        private void txtbx_src_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            Properties.Settings.Default.SourcePath = txtbx_src.Text;
            Properties.Settings.Default.Save();
        }
    }
}

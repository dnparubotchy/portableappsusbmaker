﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Management;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Collections;

namespace UsbMaker
{
    class Controller
    {

        const int BUFF_SIZE = 524288000; // Starting copy buffer size, 500 MB in bytes

        /// <summary>
        /// Gets a collection of removeable FAT32 volumes
        /// Used to fill the list box in our UI
        /// </summary>
        /// <returns>ObseravableCollection of UsbVolume objects</returns>
        public static ObservableCollection<UsbVolume> GetUsbVolumes()
        {
            ObservableCollection<UsbVolume> usbVolumes = new ObservableCollection<UsbVolume>();
            // DriveType 2 is the "Removeable" type, so we are searching for removeable volumes formatted as FAT32
            String usbQuery = "SELECT * FROM Win32_Volume WHERE DriveType = 2 AND FileSystem = 'FAT32'"; 
            ManagementObjectSearcher usbSearcher = new ManagementObjectSearcher("\\ROOT\\cimv2", usbQuery);
            foreach (ManagementObject vol in usbSearcher.Get())
            {
                usbVolumes.Add(new UsbVolume(vol));

            }
            return usbVolumes;
        }


        /// <summary>
        /// Copy a directory (and all subdirectories) from one path to N paths
        /// </summary>
        /// <param name="srcRoot">Path to source directory</param>
        /// <param name="dstRoots">Array of destination directory root paths</param>
        /// <param name="progress">Progress indicator. Can be null if progress isn't desired</param>
        /// <returns></returns>
        public static async Task CopyToMany(string srcRoot, List<string> dstRoots, IProgress<Tuple<string,int>> progress)
        {
            var srcDirStack = new Stack<string>();
            var dstDirStack = new Stack<string>();

            int read;
            int previousFileSize = 0;
            byte[] buffer = new byte[BUFF_SIZE];

            srcDirStack.Push(srcRoot);
            dstDirStack.Push("");

            while (srcDirStack.Count > 0)
            {
                DirectoryInfo srcDirInfo = new DirectoryInfo(srcDirStack.Pop());
                string dstDir = dstDirStack.Pop();

                // Create destination directories
                CreateDestDirs(dstRoots, dstDir);

                foreach (FileInfo file in srcDirInfo.GetFiles())
                {
                    // Give Progress report
                    if (progress != null)
                    {
                        progress.Report((new Tuple<string, int>(file.Name, previousFileSize)));
                    }

                    // Read file into the buffer
                    using (var srcF = new FileStream(file.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {

                        // Grow the buffer if necessary
                        // we aren't ever copying files larger than a few GB,so creating a buffer the same size as the file should be fine
                        if (file.Length > buffer.Length)
                        {
                            buffer = new byte[file.Length];
                        }

                        while ((read = srcF.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            // Write the buffer to each destination in parallel
                            await Task.Run(() =>{ WriteBufferParallel(dstRoots, Path.Combine(dstDir, file.Name), buffer, read); });
                        }
                    }

                    // update file size for our progress report
                    previousFileSize = (int)file.Length;
                }

                // Push subdirectories onto the stack
                foreach (DirectoryInfo dir in srcDirInfo.GetDirectories())
                {
                    srcDirStack.Push(dir.FullName);
                    dstDirStack.Push(Path.Combine(dstDir, dir.Name));
                }

            }

        }

        /// <summary>
        /// Private helper method that writes our file read buffer out in parallel
        /// </summary>
        /// <param name="dstRoots">Array of destination directory root paths</param>
        /// <param name="dstPath">Path to the destination file (excluding the root)</param>
        /// <param name="buffer">Buffer to be written out</param>
        /// <param name="bytesToWrite">Number of bytes to write out</param>
        private static void WriteBufferParallel(List<string> dstRoots, string dstPath, byte[] buffer, int bytesToWrite)
        {
            Parallel.ForEach(dstRoots, dstRoot =>
            {
                using (var dstF = new FileStream(Path.Combine(dstRoot, dstPath), FileMode.Create))
                {
                    Console.WriteLine(dstF.Name);

                    dstF.Write(buffer, 0, bytesToWrite);
                }

            });

        }

        public static async Task FormatUsbVolumes(IList volumes, IProgress<Tuple<string, int>> progress)
        {
            foreach (UsbVolume vol in volumes)
            {
                if (progress != null)
                {
                    progress.Report(new Tuple<string, int>(vol.DriveLetter, 1));
                }
                await Task.Run(() =>
                {
                    vol.Format();
                });
            }
        }


        /// <summary>
        /// Creates Directories in the destinations
        /// </summary>
        /// <param name="dstRoots"></param>
        /// <param name="dstPath"></param>
        private static void CreateDestDirs(List<string> dstRoots, string dstPath)
        {
                // Create destination directoris
                foreach (string dstRoot in dstRoots)
                {
                    Directory.CreateDirectory(Path.Combine(dstRoot, dstPath));
                }
        }



        // Shamelessly stole this method from Stack Overflow, I'm actually pretty surprised there isn't a Win API for this. Go figure
        public static long GetDirectorySize(string folderPath)
        {
            DirectoryInfo di = new DirectoryInfo(folderPath);
            return di.EnumerateFiles("*", SearchOption.AllDirectories).Sum(fi => fi.Length);
        }

    }
}

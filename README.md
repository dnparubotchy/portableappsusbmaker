## Portable Apps USB Maker

At my job we were constantly formatting USB sticks and copying our portable tools to them, to ensure no client computers were contaminted by any malware from another client computer.

I made this to save myself and the other technicians time and to save us the tedious process of formatting a bunch of USB sticks by hand and then getting a whole lot of simultaneous copies going. This also copies more effeciently as it reads files once before writing the buffer out to multiple destinations.

Is selective and will only format and copy to removeable volumes that are already formatted with FAT32. This is because our USBs were used in both Windows and OSX computers so they were already formatted as FAT32 and this restriction made it less likely someone would format any volumes they shouldn't.

## Usage Notes

_Note that this program must be run as an Administrator because otherwise you won't have permissions to format the volumes._

## To Do
Since this was made as a tool to be used entirely by technical staff as a time saver and not intended as a feature complete application there are a bunch of improvements that I've been meaning to make in my own time. In no particular order

### Better Error Handling
Since everyone using this wasn't going to panic at the sight of a stack trace if anything goes wrong it just spits out the exception info and then exits. Some errors however are potentially recoverable (i.e. if you can't format one of the selected volumes just ignore that volume) and it would be nice to have a more robust application

### Catching the Insertion/Removal of Volumes
Currently it only detects volumes that are connected when the program is run. It would be better to have it detect when relevent volumes are inserted or removed and update the list box of selectable volumes accordingly.
	